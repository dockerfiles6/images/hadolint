#!/usr/bin/env sh
export PRODUCT=hadolint
export SOURCE=hadolint/hadolint

pip install lastversion --user

export VERSION=$(lastversion ${SOURCE})

docker build --build-arg VERSION=${VERSION} -t ${REGISTRY}/${PRODUCT}:${VERSION} .
